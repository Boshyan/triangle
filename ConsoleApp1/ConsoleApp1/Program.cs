﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 3;
            int y = 21;
            for (int i = 0; i <= 20; i++)
            {
                Console.Write(new string(' ', x));
                Console.WriteLine(new string('*', y));

                x++;
                if (y >= 2)
                {
                    y -= 2;
                }
                else
                {
                    break;
                }


            }
        }
    }
}
